# newpy
Quickly and easily a new python file, class or module.

There are several good places that document how to properly structure a python project. With this module we aim to do all that work for you - so you can start building your project immediately!

Created by Edward

## Install

## Getting Started
Use newpy like a normal python module, ```python -m newpy PROJECT_NAME```

### Flags
```-f```
force an overwrite of any existing directory

```-d ""```
a description to be used in the generated files

```-a ""```
an author to be used in the generated files

```-l```
create a Logger to be used in the project

## Examples

```python -m newpy "mynewproject"```
The most basic way of running it; create a simple project structure with a script inside

```python -m newpy "mynewproject" -f -d "A tool to perform foo on bars"```
